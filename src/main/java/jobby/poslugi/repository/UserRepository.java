package jobby.poslugi.repository;

import org.springframework.data.repository.CrudRepository;
import jobby.poslugi.models.User;

public interface UserRepository extends CrudRepository<User, Long> {

}