package jobby.poslugi.repository;

import org.springframework.data.repository.CrudRepository;

import jobby.poslugi.models.Place;

public interface PlaceRepository extends CrudRepository<Place, Integer> {

}
