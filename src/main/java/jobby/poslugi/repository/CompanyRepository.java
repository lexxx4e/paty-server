package jobby.poslugi.repository;

import org.springframework.data.repository.CrudRepository;

import jobby.poslugi.models.Company;

public interface CompanyRepository extends CrudRepository<Company, Integer> {
	
}
