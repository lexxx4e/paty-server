package jobby.poslugi.repository;

import org.springframework.data.repository.CrudRepository;

import jobby.poslugi.models.Category;

public interface CategoryRepository extends CrudRepository<Category, Integer> {

}
