package jobby.poslugi.repository;

import org.springframework.data.repository.CrudRepository;

import jobby.poslugi.models.Chat;

public interface ChatRepository  extends CrudRepository<Chat, Integer>  {

}
