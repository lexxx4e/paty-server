package jobby.poslugi.repository;

import org.springframework.data.repository.CrudRepository;

import jobby.poslugi.models.Comment;

public interface CommentRepository extends CrudRepository<Comment, Integer> {

}
