package jobby.poslugi.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Chat {
	 	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
		
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
		name = "chat_user", 
		joinColumns = @JoinColumn(
				name = "chat_id", 
				referencedColumnName = "id"), 
		inverseJoinColumns = @JoinColumn(
				name = "user_id", referencedColumnName = "id"))
	private List<User> users;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}
   
	    
	    
}
