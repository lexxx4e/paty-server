package jobby.poslugi.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Category {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@NotNull
	private String titleRu;
	
	@NotNull
	private String titleEn;
	
	private Integer status = STATUS_NEW;
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitleRu() {
		return titleRu;
	}

	public void setTitleRu(String titleRu) {
		this.titleRu = titleRu;
	}

	public String getTitleEn() {
		return titleEn;
	}

	public void setTitleEn(String titleEn) {
		this.titleEn = titleEn;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public static final Integer STATUS_NEW = 0;

	public static final Integer STATUS_NO_ACTIVE = 10;
	
	public static final Integer STATUS_ACTIVE = 20;
	
	public static final Integer STATUS_BANNED = 30;
	
	
	
}
