package jobby.poslugi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jobby.poslugi.models.User;
import jobby.poslugi.repository.UserRepository;

@RestController
@EnableAutoConfiguration
public class MainController {
	
	@Autowired 
	private UserRepository userRepository;

	@GetMapping(path="/add") 
	public @ResponseBody String addNewUser (@RequestParam String name, @RequestParam String email) 
	{
		User n = new User();
		n.setName(name);
		n.setEmail(email);
		userRepository.save(n);
		return "Saved";
	}

	@GetMapping(path="/all")
	public @ResponseBody Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}

}
