package jobby.poslugi.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jobby.poslugi.models.Comment;
import jobby.poslugi.repository.CommentRepository;

@RestController
@RequestMapping("/comment")
public class CommentController {
	
	@Autowired 	
	private CommentRepository commentRepository;
	
	@PostMapping(path="/add")
	public @ResponseBody String addNewCompany(@RequestBody Comment c) 
	{
		c.setCreated(new Date());
		commentRepository.save(c);
		
		return "Saved";
	}

	@PostMapping(path="/all")
	public @ResponseBody Iterable<Comment> getAllComments() {
		return commentRepository.findAll();
	}
}
