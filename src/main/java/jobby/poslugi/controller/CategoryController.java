package jobby.poslugi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jobby.poslugi.models.Category;
import jobby.poslugi.repository.CategoryRepository;

@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired 	
	private CategoryRepository categoryRepository;
	
	@PostMapping(path="/add")
	public @ResponseBody String addNewCompany(@RequestBody Category c) 
	{
		categoryRepository.save(c);
		
		return "Saved";
	}

	@PostMapping(path="/all")
	public @ResponseBody Iterable<Category> getAllComments() {
		return categoryRepository.findAll();
	}
	
}
