package jobby.poslugi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jobby.poslugi.models.Place;
import jobby.poslugi.repository.PlaceRepository;

@RestController
@RequestMapping("/place")
public class PlaceController {

	@Autowired 
	private PlaceRepository placeRepository;
	
	@PostMapping(path="/add") 
	public @ResponseBody String addNewCompany(@RequestBody Place p) 
	{
		placeRepository.save(p);
		
		return "Saved";
	}

	@PostMapping(path="/all")
	public @ResponseBody Iterable<Place> getAllPlaces() {
		return placeRepository.findAll();
	}
	
}
