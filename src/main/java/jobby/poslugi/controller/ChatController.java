package jobby.poslugi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jobby.poslugi.models.Chat;
import jobby.poslugi.repository.ChatRepository;

@RestController
@RequestMapping("/chat")
public class ChatController {
	
	@Autowired 	
	private ChatRepository chatRepository;
	
	@PostMapping(path="/add")
	public @ResponseBody String addNewCompany(@RequestBody Chat c) 
	{
		chatRepository.save(c);
		
		return "Saved";
	}

	@PostMapping(path="/all")
	public @ResponseBody Iterable<Chat> getAllChats() {
		return chatRepository.findAll();
	}
}
