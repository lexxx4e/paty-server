package jobby.poslugi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jobby.poslugi.models.Company;
import jobby.poslugi.repository.CompanyRepository;

@RestController
@RequestMapping("/company")
public class CompanyController {

	@Autowired 
	private CompanyRepository companyRepository;
	
	@PostMapping(path="/add") 
	public @ResponseBody String addNewCompany(@RequestBody Company c) 
	{
		companyRepository.save(c);
		
		return "Saved";
	}

	@PostMapping(path="/all")
	public @ResponseBody Iterable<Company> getAllCompanties() {
		return companyRepository.findAll();
	}
	
}
